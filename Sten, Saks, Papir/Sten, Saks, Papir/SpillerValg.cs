﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sten__Saks__Papir
{
    class SpillerValg
    {
        public static int spillerTalValg(string x)
        {
            int valg = 0;
            bool ikkeTalValg = Int32.TryParse(x,out valg);
            while(ikkeTalValg == false || valg < 1|| valg > 3)
            {
                Console.WriteLine("Dette er ikke et gyldigt valg, prøv venligst igen");

                ikkeTalValg = Int32.TryParse(Console.ReadLine(), out valg);
            }

            return valg;
        }
    }
}
