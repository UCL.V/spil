﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sten__Saks__Papir
{
    public class Sten__Saks__Papir
    {
        public static string StenSaksPapir()

        {
            int score1 = 0;
            int score2 = 0;
            string resultat = "hej";
            string programSlut = "ja";
            while (programSlut == "ja")
            {
                
                Console.WriteLine("Dette er Sten, Saks, Papir");
                Console.WriteLine("Spiller 1 skal nu vælge, spiller 2 se venligtst ikke på hvad der bliver valgt");
                Console.WriteLine("Skriv nummeret for at vælge");
                Console.WriteLine("1: Sten");
                Console.WriteLine("2: Saks");
                Console.WriteLine("3: Papir");
                int spiller1Valg = 0;
                while (spiller1Valg < 1 || spiller1Valg > 3)

                {


                    spiller1Valg = SpillerValg.spillerTalValg(Console.ReadLine());
                }

                /*int x = 0;
                while (x < 30)
                {
                    Console.WriteLine();
                    x++;
                }*/
                Console.Clear();
                Console.WriteLine("Spiller 2 skal nu vælge");
                Console.WriteLine("1: Sten");
                Console.WriteLine("2: Saks");
                Console.WriteLine("3: Papir");
                int spiller2Valg = 0;
                while (spiller2Valg < 1 || spiller2Valg > 3)

                {

                    spiller2Valg = SpillerValg.spillerTalValg(Console.ReadLine());

                }
                string spiller1Valgt = "ikke valgt";
                string spiller2Valgt = "ikke valgt";
                string Udfald = "intet resultat";
                
                if (spiller1Valg == 1)
                {
                    spiller1Valgt = "Sten";
                    if (spiller2Valg == 1)
                    {
                        Udfald = "uafgjort";
                        spiller2Valgt = "Sten";

                    }
                    else if (spiller2Valg == 2)
                    {
                        Udfald = "Spiller 1 vinder";
                        spiller2Valgt = "Saks";
                        score1++;
                    }
                    else if (spiller2Valg == 3)
                    {
                        Udfald = "Spiller 2 vinder";
                        spiller2Valgt = "Papir";
                        score2++;
                    }
                }
                else if (spiller1Valg == 2)
                {
                    spiller1Valgt = "Saks";
                    if (spiller2Valg == 1)
                    {
                        Udfald = "Spiller 2 vinder";
                        spiller2Valgt = "Sten";
                        score2++;
                    }
                    else if (spiller2Valg == 2)
                    {
                        Udfald = "uafgjort";
                        spiller2Valgt = "Saks";
                    }
                    else if (spiller2Valg == 3)
                    {
                        Udfald = "Spiller 1 vinder";
                        spiller2Valgt = "Papir";
                        score1++;
                    }
                }
                else if (spiller1Valg == 3)
                {
                    spiller1Valgt = "Papir";
                    if (spiller2Valg == 1)
                    {
                        Udfald = "Spiller 1 vinder";
                        spiller2Valgt = "Sten";
                        score1++;
                    }
                    else if (spiller2Valg == 2)
                    {
                        Udfald = "Spiller 2 vinder";
                        spiller2Valgt = "Saks";
                        score2++;
                    }
                    else if (spiller2Valg == 3)
                    {
                        Udfald = "uafgjort";
                        spiller2Valgt = "Papir";
                    }
                }
                Console.Clear();
                Console.WriteLine("Spiller 1 valgte {0}, spiller 2 valgte {1}", spiller1Valgt, spiller2Valgt);
                Console.WriteLine("Resultat er: {0}", Udfald);
                Console.WriteLine("Scoren er:\nSpiller 1: {0} point\nSpiller 2: {1} point", score1, score2);
                Console.WriteLine("Hvis i vil spille videre, indtast ja");
                programSlut = Console.ReadLine();
                Console.Clear();

            }
            return resultat;
        }
    }
}
