﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrydsOgBolle
{
    class KrydsBolle
    {
        public static int fem()
        {
            string X = " X ";
            string O = " O ";
            string tabel1 = " 1 ";
            string tabel2 = " 2 ";
            string tabel3 = " 3 ";
            string tabel4 = " 4 ";
            string tabel5 = " 5 ";
            string tabel6 = " 6 ";
            string tabel7 = " 7 ";
            string tabel8 = " 8 ";
            string tabel9 = " 9 ";
            int spiller = 1;
            int spillertræk = 0;
            int vinder = 0;
            int valg = 0;
            bool sammetal = false;
            Console.WriteLine("Velkommen til Kryds og Bolle");
            Console.WriteLine(" _________________");
            Console.WriteLine("|     |     |     |");
            Console.WriteLine("| {0} | {1} | {2} |", tabel1, tabel2, tabel3);
            Console.WriteLine("|_____|_____|_____|");
            Console.WriteLine("|     |     |     |");
            Console.WriteLine("| {0} | {1} | {2} |", tabel4, tabel5, tabel6);
            Console.WriteLine("|_____|_____|_____|");
            Console.WriteLine("|     |     |     |");
            Console.WriteLine("| {0} | {1} | {2} |", tabel7, tabel8, tabel9);
            Console.WriteLine("|_____|_____|_____|");
            while (vinder == 0)
            {
                Console.WriteLine("Spiller {0} indtast nummeret hvor du vil placere dit træk", spiller);
            
                do
                {
                    valg = SpillerValg.spillerTalValg(Console.ReadLine());
                    if (valg == 1)
                    {
                        if (tabel1 == O || tabel1 == X)
                        {
                            Console.WriteLine("{0} er allerede valgt. Vælg et nyt", tabel1);
                            sammetal = false;
                        }


                        else if (spiller == 1)
                        {
                            tabel1 = X;
                            sammetal = true;
                        }
                        else
                        {
                            tabel1 = O;
                            sammetal = true;
                        }

                    }

                    else if (valg == 2)
                    {
                        if (tabel2 == O || tabel2 == X)
                        {
                            Console.WriteLine("{0} er allerede valgt. Vælg et nyt", tabel2);
                            sammetal = false;
                        }


                        else if (spiller == 1)
                        {
                            tabel2 = X;
                            sammetal = true;
                        }
                        else
                        {
                            tabel2 = O;
                            sammetal = true;
                        }
                    }
                    else if (valg == 3)
                    {
                        if (tabel3 == O || tabel3 == X)
                        {
                            Console.WriteLine("{0} er allerede valgt. Vælg et nyt", tabel3);
                            sammetal = false;
                        }


                        else if (spiller == 1)
                        {
                            tabel3 = X;
                            sammetal = true;
                        }
                        else
                        {
                            tabel3 = O;
                            sammetal = true;
                        }
                    }
                    else if (valg == 4)
                    {
                        if (tabel4 == O || tabel4 == X)
                        {
                            Console.WriteLine("{0} er allerede valgt. Vælg et nyt", tabel4);
                            sammetal = false;
                        }


                        else if (spiller == 1)
                        {
                            tabel4 = X;
                            sammetal = true;
                        }
                        else
                        {
                            tabel4 = O;
                            sammetal = true;
                        }
                    }
                    else if (valg == 5)
                    {
                        if (tabel5 == O || tabel5 == X)
                        {
                            Console.WriteLine("{0} er allerede valgt. Vælg et nyt", tabel5);
                            sammetal = false;
                        }


                        else if (spiller == 1)
                        {
                            tabel5 = X;
                            sammetal = true;
                        }
                        else
                        {
                            tabel5 = O;
                            sammetal = true;
                        }
                    }
                    else if (valg == 6)
                    {
                        if (tabel6 == O || tabel6 == X)
                        {
                            Console.WriteLine("{0} er allerede valgt. Vælg et nyt", tabel6);
                            sammetal = false;
                        }


                        else if (spiller == 1)
                        {
                            tabel6 = X;
                            sammetal = true;
                        }
                        else
                        {
                            tabel6 = O;
                            sammetal = true;
                        }
                    }
                    else if (valg == 7)
                    {
                        if (tabel7 == O || tabel7 == X)
                        {
                            Console.WriteLine("{0} er allerede valgt. Vælg et nyt", tabel7);
                            sammetal = false;
                        }


                        else if (spiller == 1)
                        {
                            tabel7 = X;
                            sammetal = true;
                        }
                        else
                        {
                            tabel7 = O;
                            sammetal = true;
                        }
                    }
                    else if (valg == 8)
                    {
                        if (tabel8 == O || tabel8 == X)
                        {
                            Console.WriteLine("{0} er allerede valgt. Vælg et nyt", tabel8);
                            sammetal = false;
                        }


                        else if (spiller == 1)
                        {
                            tabel8 = X;
                            sammetal = true;
                        }
                        else
                        {
                            tabel8 = O;
                            sammetal = true;
                        }
                    }
                    else if (valg == 9)
                    {
                        if (tabel9 == O || tabel9 == X)
                        {
                            Console.WriteLine("{0} er allerede valgt. Vælg et nyt", tabel9);
                            sammetal = false;
                        }
                        else if (spiller == 1)
                        {
                            tabel9 = X;
                            sammetal = true;
                        }
                        else
                        {
                            tabel9 = O;
                            sammetal = true;
                        }
                    }
                } while (sammetal == false);
                if (tabel1 == tabel2 && tabel1 == tabel3 || tabel1 == tabel4 && tabel1 == tabel7 ||
                tabel1 == tabel5 && tabel1 == tabel9 || tabel2 == tabel5 && tabel2 == tabel8 ||
                tabel3 == tabel6 && tabel3 == tabel9 || tabel3 == tabel5 && tabel3 == tabel7 ||
                tabel4 == tabel5 && tabel4 == tabel6 || tabel7 == tabel8 && tabel7 == tabel9
                )
                {
                    if (spiller == 1)
                    {
                        vinder = 1;
                    }
                    else
                    {
                        vinder = 2;
                    }
                    break;
                }
                if (spillertræk == 8)
                {
                    vinder = 3;
                    break;
                }
                Console.Clear();
                Console.WriteLine(" _________________");
                Console.WriteLine("|     |     |     |");
                Console.WriteLine("| {0} | {1} | {2} |", tabel1, tabel2, tabel3);
                Console.WriteLine("|_____|_____|_____|");
                Console.WriteLine("|     |     |     |");
                Console.WriteLine("| {0} | {1} | {2} |", tabel4, tabel5, tabel6);
                Console.WriteLine("|_____|_____|_____|");
                Console.WriteLine("|     |     |     |");
                Console.WriteLine("| {0} | {1} | {2} |", tabel7, tabel8, tabel9);
                Console.WriteLine("|_____|_____|_____|");
                Console.WriteLine("Spiller {0} valgte {1}", spiller, valg);
                if (spiller == 1)
                {
                    spiller = 2;
                }
                else
                {
                    spiller = 1;
                }
                spillertræk++;
            }
            Console.Clear();
            Console.WriteLine(" _________________");
            Console.WriteLine("|     |     |     |");
            Console.WriteLine("| {0} | {1} | {2} |", tabel1, tabel2, tabel3);
            Console.WriteLine("|_____|_____|_____|");
            Console.WriteLine("|     |     |     |");
            Console.WriteLine("| {0} | {1} | {2} |", tabel4, tabel5, tabel6);
            Console.WriteLine("|_____|_____|_____|");
            Console.WriteLine("|     |     |     |");
            Console.WriteLine("| {0} | {1} | {2} |", tabel7, tabel8, tabel9);
            Console.WriteLine("|_____|_____|_____|");
            Console.WriteLine("Spiller {0} valgte {1}", spiller, valg);
            if (vinder == 1 || vinder == 2)
            {
                Console.WriteLine("Spiller {0} vinder!", spiller);
            }
            else
            {
                Console.WriteLine("Spillet er uafgjort");
            }

                return 5;
        }
    }
}
